# Aseprite Flatpak

Aseprite is an animated sprite and pixel tool that is packaged using flatpak



## Build Steps
git clone
cd aseprite-flatpak
flatpak-builder --user --install --install-deps-from=flathub builder org.aseprite.aseprite.json
